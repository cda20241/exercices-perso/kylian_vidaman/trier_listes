import random

liste_num = [0,1,2,3,4,5,6,7,8,9]

liste = [
    ["Pascal", "ROSE", 43],
    ["Mickaël", "FLEUR", 29],
    ["Henry", "TULIPE", 35],
    ["Michel", "FRAMBOISE", 35],
    ["Arthur", "PETALE", 35],
    ["Michel", "POLLEN", 50],
    ["Maurice", "FRAMBOISE", 42]
]

random.shuffle(liste_num)
random.shuffle(liste)

def partitions(list, min, max, param) :
    """
        Récupère une liste, prend l'element en fin de liste comme pivot, est comparé au reste de la liste
    Args:
        list ([list ([ str, str, int])]): liste de personne avec leur nom, prenom et age
        min ([int]): première itération de la partition
        max ([int]): dernière itération de la partition
        param ([int]) : type de data qu'on veut extraire dans notre liste de personne

    Returns:
        [int]: renvoi l'index du pivot de la liste 
    """
    piv = list[max][param]
    i = min -1
    # on parcourt toute notre liste
    for ref in range(min, max):
        if list[ref][param] <= piv:
            # si notre pivot est supérieur ou égale à la valeur à l'index j de la liste
            # on incrémente i, puis on échange l'index j avec 
            i += 1
            (list[i], list[ref]) = (list[ref], list[i])
    (list[i+1], list[max]) = (list[max], list[i+1])
    return i+1

def partitions_4(list, min, max, param) :
    """
        Récupère une liste, prend l'element en fin de liste comme pivot, est comparé au reste de la liste
    Args:
        list ([list ([ str, str, int])]): liste de personne avec leur nom, prenom et age
        min ([int]): première itération de la partition
        max ([int]): dernière itération de la partition
        param ([int]) : type de data qu'on veut extraire dans notre liste de personne

    Returns:
        [int]: renvoi l'index du pivot de la liste 
    """
    piv = list[max][param]
    i = min -1
    # on parcourt toute notre liste
    for ref in range(min, max): 
            if list[ref][param] < piv:
                # si notre pivot est supérieur ou égale à la valeur à l'index j de la liste
                # on incrémente i, puis on échange l'index j avec 
                i += 1
                (list[i], list[ref]) = (list[ref], list[i])
            elif list[ref][param] == piv :
                if list[ref][param-1] <= list[max][param-1]:
                    # si notre pivot est supérieur ou égale à la valeur à l'index j de la liste
                    # on incrémente i, puis on échange l'index j avec 
                    i += 1
                    (list[i], list[ref]) = (list[ref], list[i])
    (list[i+1], list[max]) = (list[max], list[i+1])
    return i+1

def quicksort(list, min, max, param) :
    """ Fonction récursive de tri
    Args:
        list ([list ([ str, str, int])]): liste de personne avec leur nom, prenom et age
        min ([int]): première itération de la liste
        max ([int]): dernière itération de la liste
        param ([int]) : type de data qu'on veut extraire dans notre liste de personne
    """
    if min < max : 
        # si la list n'est pas vide
        pi = partitions(list, min, max, param)
        # on utilise la récursivité avec la liste avant le pivot d'un côté, puis avec le reste de la liste après le pivot
        quicksort(list, min, pi-1, param)
        quicksort(list, pi+1, max, param)

def quicksort_4(list, min, max, param) :
    """ Fonction récursive de tri
    Args:
        list ([list ([ str, str, int])]): liste de personne avec leur nom, prenom et age
        min ([int]): première itération de la liste
        max ([int]): dernière itération de la liste
        param ([int]) : type de data qu'on veut extraire dans notre liste de personne
    """
    if min < max : 
        # si la list n'est pas vide
        pi = partitions_4(list, min, max, param)
        # on utilise la récursivité avec la liste avant le pivot d'un côté, puis avec le reste de la liste après le pivot
        quicksort(list, min, pi-1, param)
        quicksort(list, pi+1, max, param)

def liste_1(liste) :
    """Prend une liste en paramètre puisla trie par ordre alphabétique des noms
    Args:
        list ([list ([ str, str, int])]): liste de personne avec leur nom, prenom et age
    """
    quicksort(liste, 0, len(liste)-1, 1)

    print("## Liste 1 : tri par ordre alphabétique des noms ##\n")
    for personne in liste :
        print(personne[0] + " " + personne[1] + " " + str(personne[2]))
    print("\n## FIN ##")

def liste_2(liste) :
    """Prend une liste en paramètre puis la trie par ordre alphabétique inversé des noms
    Args:
        list ([list ([ str, str, int])]): liste de personne avec leur nom, prenom et age
    """
    quicksort(liste, 0, len(liste)-1, 1)
    liste.reverse()
    print("## Liste 2 : tri par ordre alphabétique inversé des noms ##\n")
    for personne in liste :
        print(personne[0] + " " + personne[1] + " " + str(personne[2]))
    print("\n## FIN ##")
 
def liste_3(liste) :
    """Prend une liste en paramètre puisla trie par ordre alphabétique des âges
    Args:
        list ([list ([ str, str, int])]): liste de personne avec leur nom, prenom et age
    """
    quicksort(liste, 0, len(liste)-1, 2)
    print("## Liste 3 : tri par ordre croissant des âges ##\n")
    for personne in liste :
        print(personne[0] + " " + personne[1] + " " + str(personne[2]))
    print("\n## FIN ##")
 
def liste_4(liste) :
    """Prend une liste en paramètre puisla trie par ordre alphabétique des noms, puis des prénoms
        si ils sont identiques
    Args:
        list ([list ([ str, str, int])]): liste de personne avec leur nom, prenom et age
    """
    quicksort_4(liste, 0, len(liste)-1, 1)
    print("## Liste 4 : tri par ordre alphabetique des noms, puis prénoms ##\n")
    for personne in liste :
        print(personne[0] + " " + personne[1] + " " + str(personne[2]))
    print("\n## FIN ##")

liste_1(liste)
liste_2(liste)
liste_3(liste)
liste_4(liste)